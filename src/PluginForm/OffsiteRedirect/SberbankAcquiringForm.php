<?php

namespace Drupal\commerce_sberbank_acquiring\PluginForm\OffsiteRedirect;

use Drupal;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Voronkovich\SberbankAcquiring\Client as SberbankClient;
use Voronkovich\SberbankAcquiring\Exception\ActionException;
use Voronkovich\SberbankAcquiring\HttpClient\GuzzleAdapter as SberbankGuzzleAdapter;

/**
 * Order registration and redirection to payment URL.
 */
class SberbankAcquiringForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The currency storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $currencyStorage;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * The current language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  private $currentLanguage;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * SberbankAcquiringForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $currency_storage
   *   The currency storage.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Language\LanguageInterface $current_language
   *   The current language.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    EntityStorageInterface $currency_storage,
    ClientInterface $http_client,
    LanguageInterface $current_language,
    LoggerChannelInterface $logger,
    ModuleHandlerInterface $module_handler
  ) {
    $this->currencyStorage = $currency_storage;
    $this->httpClient = $http_client;
    $this->currentLanguage = $current_language;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('commerce_currency'),
      $container->get('http_client'),
      $container->get('language_manager')->getCurrentLanguage(),
      $container->get('logger.channel.commerce_sberbank_acquiring'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    // If payment is not have ID, we force save it, since it used in $order_id
    // which is required for payment.
    if ($payment->isNew()) {
      $payment->save();
    }

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configs = $payment_gateway_plugin->getConfiguration();
    // Get username and password for payment method.
    $username = $configs['username'];
    $password = $configs['password'];
    $currency_code = $payment->getAmount()->getCurrencyCode();
    /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
    $currency = $this->currencyStorage->load($currency_code);

    // Set REST API url for test or live modes.
    switch ($this->plugin->getMode()) {
      default:
      case 'test':
        $api_uri = SberbankClient::API_URI_TEST;
        break;

      case 'live':
        $api_uri = SberbankClient::API_URI;
        break;
    }

    // Prepare client to be executed.
    $client = new SberbankClient([
      'userName' => $username,
      'password' => $password,
      'language' => $this->currentLanguage->getId(),
      // ISO 4217 currency code.
      'currency' => $currency->getNumericCode(),
      'apiUri' => $api_uri,
      'httpClient' => new SberbankGuzzleAdapter($this->httpClient),
    ]);

    // Set additional params to order.
    // We use Payment ID instead of actual order id, because sberbank allows to
    // register payment with same 'order_id' once. So if something wrong
    // happens, user can't pay for this order again using this payment method.
    // So Payment ID is more reliable, because every try it will have new ID,
    // and we can get order by payment_id.
    $order_id = $configs['order_id_prefix'] . $payment->id() . $configs['order_id_suffix'];
    $order_amount = (int) ($payment->getAmount()->getNumber() * 100);

    $params = [
      'failUrl' => $form['#cancel_url'],
    ];

    $context = [
      'payment' => $payment,
    ];
    $this->moduleHandler->alter('commerce_sberbank_acquiring_register_order', $params, $context);

    // Execute request to Sberbank.
    try {
      $result = $client->registerOrder($order_id, $order_amount, $form['#return_url'], $params);

      $payment->setAuthorizedTime(time());
      $payment->setRemoteId($result['orderId']);
      $payment->setState('authorization');
      $payment->save();
    }
    catch (ActionException $exception) {
      // If something goes wrong, we stop payment and show error for it.
      $this->logger->error("Payment for order #@order_id is throws an error. Message: @message", [
        '@order_id' => $payment->getOrderId(),
        '@message' => $exception->getMessage(),
      ]);

      // Mark payment is failed.
      $payment->setState('authorization_voided');
      $payment->save();

      throw new PaymentGatewayException();
    }

    $payment_form_url = $result['formUrl'];

    return $this->buildRedirectForm($form, $form_state, $payment_form_url, [], self::REDIRECT_GET);
  }

}
